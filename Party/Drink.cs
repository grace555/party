﻿using System;
using SQLite;
using System.Collections.Generic;
using System.Linq;

namespace Party
{
	public class Drink
	{
		public Drink ()
		{
		}

		[Ignore] 
		public List<int> Ingridients{
			get{ 
				List<int> l = new List<int> (); 
				foreach (string i in ingridients.Split (' ')) 
					l.Add (Int32.Parse(i)); 
				return l;
			} 
			set{ } 
		}
			
		public int Level { get; set;} 

		[PrimaryKey, AutoIncrement, NotNull] 
		public int ID { get; set; } 

		[NotNull] 
		public string Name { get; set; } 

		[NotNull] 
		public string ingridients { get; set; } 

		[NotNull] 
		public string ImageName{ get; set; }
	}
}
