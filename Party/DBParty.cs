﻿using System;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Party
{
	public class DBParty
	{
		static object locker = new object (); 

		SQLiteConnection database; 

		string DatabasePath { 
			get { 
				var sqliteFilename = "Drinks.db"; 
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder 
				var path = Path.Combine(documentsPath, sqliteFilename); 
				return path; 
			} 
		} 

		public DBParty() 
		{ 
			database = new SQLiteConnection (DatabasePath); 
			// create the tables 
			database.CreateTable<Drink>(); 
			database.CreateTable<Item>(); 
		} 

		public void Drop()
		{
			database.DropTable<Drink> ();
			database.DropTable<Item> ();
		}

		public IEnumerable<Drink> GetDrinks () 
		{ 
			lock (locker) { 
				return (from i in database.Table<Drink>() select i).ToList(); 
			} 
		} 

		public Drink GetDrink (int id) 
		{ 
			lock (locker) { 
				return database.Table<Drink>().FirstOrDefault(x => x.ID == id); 
			} 

		} 

		public int SaveDrink (Drink drink) 
		{ 
			lock (locker) { 
				if (drink.ID != 0) { 
					database.Update(drink); 
					return drink.ID; 
				} else { 
					return database.Insert(drink); 
				} 
			} 
		} 

		public int DeleteDrink(int id) 
		{ 
			lock (locker) { 
				return database.Delete<Drink>(id); 
			} 
		} 

		public IEnumerable<Item> GetItems () 
		{ 
			lock (locker) { 
				return (from i in database.Table<Item>() select i).ToList(); 
			} 
		} 

		public Item GetItem (int id) 
		{ 
			lock (locker) { 
				return database.Table<Item>().FirstOrDefault(x => x.ID == id); 
			} 
		} 

		public int SaveItem (Item item) 
		{ 
			lock (locker) { 
				if (item.ID != 0) { 
					database.Update(item); 
					return item.ID; 
				} else { 
					return database.Insert(item); 
				} 
			} 
		} 

		public int DeleteItem(int id) 
		{ 
			lock (locker) { 
				return database.Delete<Item>(id); 
			} 
		}
	}
}