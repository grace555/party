﻿using System;
using SQLite;

namespace Party
{
	public class Item
	{
		public Item ()
		{
		}

		[PrimaryKey, AutoIncrement, NotNull] 
		public int ID { get; set; } 

		[NotNull] 
		public string Name { get; set; }

		[NotNull] 
		public int Strength { get; set; } 

		[NotNull] 
		public int Level { get; set;} 

		[NotNull] 
		public string ImageName{ get; set; }
	}
}

