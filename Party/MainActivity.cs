﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Android.Net;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Party
{
	[Activity(Label = "Вечеринка", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		LinearLayout used;
		List<String> usedItems = new List<String>();
		Dictionary<String,int> usedItemsDict = new Dictionary<String, int>();
		Dictionary<int,int> drinksFromChosenIngridients = new Dictionary<int, int>();

		int drinksFromChosenIngridientsPos = -1;
		int numPeople = 1;

		private DBParty database = new DBParty();



		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			IEnumerable<Item> items = database.GetItems ();
			IEnumerable<Drink> drinks = database.GetDrinks ();

			CreateDatabase ();
			UpdateDrinksContent ();
			UpdateSmile ();


			LinearLayout panel = FindViewById<LinearLayout> (Resource.Id.ItemsPanel);
			panel.Drag += DropZone_Info;
			used = FindViewById<LinearLayout> (Resource.Id.usedItems);
			used.Drag += DropZone_Drag;

			SetUsedItemsList ();
			SetItemsImages (panel, items);


			SetButtonClickFunctions (drinks, items);
			ShowRecipe (drinks, items);

		}

		public void SetItemsImages(LinearLayout panel, IEnumerable<Item> items){
			for (int i = 0; i < items.Count (); i++) {
				ImageView image = new ImageView (ApplicationContext);
				image.SetImageResource (Resource.Drawable.item00 + i);
				image.SetPadding (15, 0, 0, 0);
				image.SetAdjustViewBounds (true);
				image.Click += Item_Click;
				image.Touch += Item_Touch;
				image.Tag = items.ElementAt (i).Name;
				panel.AddView (image);
			}
		}

		public void SetUsedItemsList (){
			ListView Itemslist = FindViewById<ListView> (Resource.Id.listView1);
			SetAdapterForUsedItemsList ();
			Itemslist.ItemClick += List_ItemClick;
		}			

		public void SetAdapterForUsedItemsList (){
			ListView Itemslist = FindViewById<ListView> (Resource.Id.listView1);
			ArrayAdapter<String> adapter = new ArrayAdapter<String> (ApplicationContext, Android.Resource.Layout.SimpleListItem1, usedItems);
			Itemslist.Adapter = adapter;
		}

		void SetButtonClickFunctions(IEnumerable<Drink> drinks, IEnumerable<Item> items){
			SelectDrink(drinks, items);
			ChooseAmountOfDrink ();
			SetNumberOfPeople ();
			ClearScreen ();
		}

		void SelectDrink(IEnumerable<Drink> drinks, IEnumerable<Item> items){
			FindViewById (Resource.Id.ButtonLeft).Click += delegate {
				drinksFromChosenIngridientsPos--;
				UpdateDrinksContent ();
			};

			FindViewById (Resource.Id.ButtonRight).Click += delegate {
				drinksFromChosenIngridientsPos++;
				UpdateDrinksContent ();
			};			
		}

		void ChooseAmountOfDrink(){
			FindViewById (Resource.Id.ButtonMinus).Click += delegate {
				MinusClick ();
			};

			FindViewById (Resource.Id.ButtonPlus).Click += delegate {
				PlusClick ();
			};
		}

		void SetNumberOfPeople(){
			FindViewById (Resource.Id.minusPerson).Click += delegate {
				if (numPeople > 1)
					numPeople--;
				UpdateSmile ();
			};

			FindViewById (Resource.Id.plusPerson).Click += delegate {
				if (numPeople < 12)
					numPeople++;
				UpdateSmile ();
			};
		}	

		void ClearScreen(){
			FindViewById (Resource.Id.button1).Click += delegate {
				usedItemsDict.Clear();
				drinksFromChosenIngridients.Clear();
				UpdateItems();
			};
		}

		void ShowRecipe(IEnumerable<Drink> drinks, IEnumerable<Item> items){
			FindViewById (Resource.Id.drinkImage).Click += delegate {
				if (drinksFromChosenIngridientsPos != -1)
				{
					Drink drink = (Drink)drinks.First(x => x.ID == drinksFromChosenIngridients.ElementAt(drinksFromChosenIngridientsPos).Key);
					string str = "";
					foreach(int i in drink.Ingridients)
						str += items.First( t => t.ID == i).Name + "\n";
					str.Remove(str.Length - 2);
					Toast.MakeText (ApplicationContext, str, ToastLength.Long).Show ();
				}
			};
		}

		void List_ItemClick (object sender, AdapterView.ItemClickEventArgs e)
		{
			int position = e.Position;
			String name = usedItems [position].Split(new String[] {"\n"}, StringSplitOptions.None) [0];;

			usedItemsDict [name]--;
			if (usedItemsDict[name] == 0)
				usedItemsDict.Remove(name);
			UpdateItems ();
		}

		void DropZone_Info (object sender, View.DragEventArgs e)
		{
			var evt = e.Event;
			switch (evt.Action) {
				case DragAction.Ended:  
				case DragAction.Started:
					e.Handled = true;
					break;                

				case DragAction.Drop:
					e.Handled = true;
					var data = e.Event.ClipData;
					String name = data.GetItemAt (0).Text;
					Toast.MakeText (ApplicationContext, name, ToastLength.Long).Show ();
					break;
			}
		}

		void DropZone_Drag (object sender, View.DragEventArgs e)
		{
			// React on different dragging events
			var evt = e.Event;
			switch (evt.Action) 
			{
				case DragAction.Ended:
					((LinearLayout)FindViewById (Resource.Id.usedItems)).SetBackgroundColor (new Color (57, 126, 130));
					e.Handled = true;
					break;
				case DragAction.Started:
					((LinearLayout)FindViewById (Resource.Id.usedItems)).SetBackgroundColor (new Color (135, 206, 250));
					e.Handled = true;
					break;                
				case DragAction.Entered:                   
					break;
				case DragAction.Exited:
					break;
				case DragAction.Drop:
					
					e.Handled = true;

					var data = e.Event.ClipData;
					if (data != null) {						
						String name = data.GetItemAt (0).Text;
						if (usedItemsDict.ContainsKey (name))
							usedItemsDict [name]++;
						else
							usedItemsDict [name] = 1;
						UpdateItems ();
					}
					break;
			}
		}

		void Item_Click (object sender, EventArgs e)
		{
			Toast.MakeText(ApplicationContext, (String)((ImageView)sender).Tag, ToastLength.Long).Show();
		}

		void Item_Touch (object sender, EventArgs e)
		{
			String name = (String)((ImageView)sender).Tag;
			var data = ClipData.NewPlainText ("name", name);
			((ImageView)sender).StartDrag (data, new View.DragShadowBuilder((ImageView)sender), null, 0);
		}

		void DictToList()
		{
			usedItems.Clear ();
			foreach (var i in usedItemsDict) {
				if (i.Value != 0)
					usedItems.Add (i.Key + "\n" + i.Value.ToString ());
			}
			usedItems.Sort ();
		}

		void UpdateItems()
		{
			DictToList ();
			UpdateDrinksContent ();
			UpdateSmile ();
			SetAdapterForUsedItemsList ();
		}

		void UpdateDrinksContent ()
		{
			UpdateDrinksFromChosenIngridients ();
			SetDrinksFromChosenIngridientsPos ();
			UpdateScreen ();
		}

		public void UpdateScreen (){
			SetRightButtonImage ();
			SetLeftButtonImage ();
			SetScreenImage ();
		}

		public void SetScreenImage (){			
			ImageButton buttonPlus = (ImageButton) FindViewById(Resource.Id.ButtonPlus);
			ImageButton buttonMinus = (ImageButton) FindViewById(Resource.Id.ButtonMinus);

			if (drinksFromChosenIngridientsPos == -1) 
				SetEmptyScreen (buttonPlus, buttonMinus);
			else 
				SetScreenDrink (buttonPlus, buttonMinus);
		}

		public void SetScreenDrink(ImageButton buttonPlus, ImageButton buttonMinus){
			IEnumerable<Drink> drinks = database.GetDrinks ();
			Drink drink = (Drink)drinks.First(x => x.ID == drinksFromChosenIngridients.ElementAt(drinksFromChosenIngridientsPos).Key);
			int imageId = Resource.Drawable.drink00 + drink.ID - 1;
			string drinkAmount = drinksFromChosenIngridients.ElementAt (drinksFromChosenIngridientsPos).Value.ToString ();
			ShowCurrentDrink(imageId, drink.Name, drinkAmount);
			CheckPlusButtonEnabled(buttonPlus, drink);
			CheckMinusButtonEnabled(buttonMinus, drink.ID);
		}

		public void ShowCurrentDrink(int imageId, string name, string amount){
			((ImageView)FindViewById (Resource.Id.drinkImage)).SetImageResource (imageId);
			((TextView)FindViewById (Resource.Id.drinkText)).Text = name;
			((TextView)FindViewById (Resource.Id.drinkCount)).Text = amount;
		}

		public void SetEmptyScreen (ImageButton buttonPlus, ImageButton buttonMinus){
			ShowCurrentDrink (Resource.Drawable.clear, "", "");
			SetButtonState(buttonPlus,Resource.Drawable.clear, false);
			SetButtonState(buttonMinus,Resource.Drawable.clear, false);
		}

		public void CheckPlusButtonEnabled(ImageButton buttonPlus, Drink drink){
			bool IsLastOneCanBeMade = CanBeMadeOneMore(drink);
			if (IsLastOneCanBeMade) 
				SetButtonState(buttonPlus,Resource.Drawable.clear, false);
			else 
				SetButtonState(buttonPlus,Resource.Drawable.plus, true);
		}

		public bool CanBeMadeOneMore(Drink drink){
			var items = database.GetItems ();
			foreach (var i in drink.Ingridients)
				if (!usedItemsDict.Keys.Contains (items.First (x => x.ID == i).Name))
					return true;
				else if (usedItemsDict [items.First (x => x.ID == i).Name] == 0)
					return true;
			return false;
		}

		public void SetButtonState(ImageButton button, int imageId, bool isEnabled){
			button.Enabled = isEnabled;
			button.SetImageResource (imageId);
		}

		public void CheckMinusButtonEnabled(ImageButton buttonMinus, int drinkId){
			if (!drinksFromChosenIngridients.Keys.Contains(drinkId) || drinksFromChosenIngridients[drinkId] == 0) 
				SetButtonState(buttonMinus,Resource.Drawable.clear, false);
			else 
				SetButtonState(buttonMinus,Resource.Drawable.minus, true);
		}

		public void UpdateDrinksFromChosenIngridients(){			
			Dictionary<int, bool> DrinksMayBeMade = FindDrinksCanBeMade();
			SetNewDrinksInDrinksDict (DrinksMayBeMade);
			RemoveNotUsedDrinksCantBeMade (DrinksMayBeMade);
		}

		public void SetNewDrinksInDrinksDict (Dictionary<int, bool> DrinksMayBeMade){
			foreach (var dr in DrinksMayBeMade)
				if (dr.Value && !(drinksFromChosenIngridients.ContainsKey (dr.Key)))
					drinksFromChosenIngridients [dr.Key] = 0;
		}

		public void RemoveNotUsedDrinksCantBeMade (Dictionary<int, bool> DrinksMayBeMade){
			for (int i = 0; i < drinksFromChosenIngridients.Count (); i++) {
				var z = DrinksMayBeMade [drinksFromChosenIngridients.ElementAt (i).Key];
				if (!z && (drinksFromChosenIngridients.ElementAt (i).Value == 0))
					drinksFromChosenIngridients.Remove (drinksFromChosenIngridients.ElementAt (i).Key);
			}
		}

		public Dictionary<int, bool> FindDrinksCanBeMade(){
			IEnumerable<Drink> AllDrinks = database.GetDrinks ();
			List<int> IdItemsUsedNow = FindItemsUsedNow().Select (x => x.ID).ToList();
			Dictionary<int, bool> resultDrinks = new Dictionary<int, bool> ();
			foreach (Drink d in AllDrinks) {
				bool contains = true;
				foreach (int ingridient in d.Ingridients)
					if (!IdItemsUsedNow.Contains (ingridient))
						contains = false;
				resultDrinks [d.ID] = contains;
			}
			return resultDrinks;
		}

		public IEnumerable<Item> FindItemsUsedNow(){
			IEnumerable<Item> items = database.GetItems ();
			return items.Where (x => usedItemsDict.Keys.Contains (x.Name));
		}

		public void SetDrinksFromChosenIngridientsPos(){
			if (drinksFromChosenIngridientsPos > drinksFromChosenIngridients.Count () - 1)
				drinksFromChosenIngridientsPos = drinksFromChosenIngridients.Count () - 1;
			if (drinksFromChosenIngridientsPos == -1 && drinksFromChosenIngridients.Count () > 0)
				drinksFromChosenIngridientsPos = 0;
			else if (drinksFromChosenIngridientsPos != -1 && drinksFromChosenIngridients.Count() == 0)
				drinksFromChosenIngridientsPos = -1;
		}

		public void SetRightButtonImage(){
			ImageButton button = (ImageButton) FindViewById (Resource.Id.ButtonRight);
			if (drinksFromChosenIngridientsPos == drinksFromChosenIngridients.Count () - 1) {
				button.Enabled = false;
				button.SetImageResource (Resource.Drawable.clear);
			} else {
				button.Enabled = true;
				button.SetImageResource (Resource.Drawable.right);
			}
		}

		public void SetLeftButtonImage(){
			ImageButton button = (ImageButton) FindViewById (Resource.Id.ButtonLeft);
			if (drinksFromChosenIngridientsPos <= 0) {
				button.Enabled = false;
				button.SetImageResource (Resource.Drawable.clear);
			} else {
				button.Enabled = true;
				button.SetImageResource (Resource.Drawable.left);
			}
		}

		void PlusClick()
		{
			Drink drink = GetCurrentDrink ();
			ChangeAmountOfCurrentDrink (1);
			DecreaseAmountOfIngridientsCurrentDrinkMadeOf (drink);
		}

		public Drink GetCurrentDrink(){
			IEnumerable<Drink> drinks = database.GetDrinks ();
			return (Drink)drinks.First(x => x.ID == CurrentDrinkId());
		}

		public int CurrentDrinkId(){
			return drinksFromChosenIngridients.ElementAt (drinksFromChosenIngridientsPos).Key;
		}

		public void ChangeAmountOfCurrentDrink(int delta){
			var v = drinksFromChosenIngridients.ElementAt (drinksFromChosenIngridientsPos);
			drinksFromChosenIngridients.Remove (v.Key);
			drinksFromChosenIngridients.Add (v.Key, v.Value + delta);
		}

		public void DecreaseAmountOfIngridientsCurrentDrinkMadeOf(Drink drink){
			IEnumerable<Item> items = database.GetItems ();
			foreach (var i in drink.Ingridients)
				usedItemsDict [items.First (x => x.ID == i).Name]--;
			UpdateItems ();
		}

		void MinusClick()
		{
			Drink drink = GetCurrentDrink ();
			ChangeAmountOfCurrentDrink (-1);
			IncreaseAmountOfIngridientsCurrentDrinkMadeOf (drink);

		}

		public void IncreaseAmountOfIngridientsCurrentDrinkMadeOf(Drink drink){
			IEnumerable<Item> items = database.GetItems ();
			foreach (var i in drink.Ingridients)
				if (usedItemsDict.Keys.Contains(items.First (x => x.ID == i).Name))
					usedItemsDict [items.First (x => x.ID == i).Name]++;
				else usedItemsDict[items.First (x => x.ID == i).Name] = 1;
			UpdateItems ();
		}

		void UpdateSmile()
		{
			for (int i = 0; i < 3; i++) {
				ImageView img = (ImageView) FindViewById (Resource.Id.smileImage0 + i);
				if (i < numPeople)
					img.SetImageResource (Resource.Drawable.smile1 + SetSmileType());
				else
					img.SetImageResource (Resource.Drawable.smile0);
				if (numPeople > 3)
					((TextView)FindViewById (Resource.Id.smileAddCount)).Text = "+ " + (numPeople - 3);
				else ((TextView)FindViewById (Resource.Id.smileAddCount)).Text = "";
			}
		}

		public int CalcutateTotalLevel(){
			return CalculateDrinksLevel() + CalculateNotUsedItemsLevel ();
		}

		public int CalculateDrinksLevel(){
			int resultLevel = 0;
			IEnumerable<Drink> drinks = database.GetDrinks ();
			for (int i = 0; i < drinksFromChosenIngridients.Count(); i++) {
				Drink drink = (Drink)drinks.First (x => x.ID == drinksFromChosenIngridients.ElementAt(i).Key);
				resultLevel += drink.Level * drinksFromChosenIngridients.ElementAt(i).Value;
			}
			return resultLevel;
		}

		public int CalculateNotUsedItemsLevel (){
			IEnumerable<Item> items = database.GetItems ();
			int resultLevel = 0;
			for (int i = 0; i < usedItemsDict.Count (); i++) {
				Item it = (Item)items.First (x => x.Name == usedItemsDict.ElementAt (i).Key);
				resultLevel += (int)Math.Round(((it.Level * 10 + it.Strength) * 0.75 * usedItemsDict.ElementAt (i).Value));
			}
			return resultLevel;
		}
			

		public int SetSmileType(){
			int FinalLevel = CalcutateTotalLevel() / numPeople;
			if (FinalLevel < 150)
				return 0;
			else if (FinalLevel < 300)
				return 1;
			else if (FinalLevel < 450)
				return 2;
			else
				return 3;
		}

		void CreateDatabase()
		{
			database.Drop ();
			database = new DBParty ();

			String[,] items = new string[,] {
				{"Вода", 				"0", "1"},
				{"Апельсиновый сок", 	"0", "2"},
				{"Кола", 				"0", "1"},
				{"Виски", 				"40", "5"},
				{"Гренадин",			"0", "3"},
				{"Лед",					"0", "1"},
				{"Сливки",				"0", "1"},
				{"Ликер",				"15", "5"},
				{"Ананасовый сок",		"0", "2"},
				{"Кокосовое молоко",	"0", "3"},
				{"Томатный сок",		"0", "2"},
				{"Лайм",				"0", "3"},
				{"Лимонный сок",		"0", "2"},
				{"Белый ром",			"35", "5"},
				{"Текила",				"32", "4"},
				{"Сок лайма",			"0", "2"},
				{"Коричневый сахар",	"0", "2"},
				{"Джин",				"45", "5"},
				{"Водка",				"40", "2"},
				{"Мята",				"0", "3"},
				{"Чай",					"0", "2"}
			};

			String[,] drinks = new string[,] {
				{"Пусс-кафе", 			"4 5 8"},
				{"Звезды и полосы", 	"5 7"},
				{"Текила санрайз", 		"2 5 15"},
				{"Пина-Коллада", 		"9 10 14"},
				{"Дайкири", 			"14 16 17"},
				{"Александр", 			"7 8 18"},
				{"Кровавая Мери", 		"6 11 19"},
				{"Напиток Джо", 		"6 12 19"},
				{"Джо Коллинз", 		"4 6 13"},
				{"Айс Пик", 			"6 19 21"},
				{"Ром-Кола", 			"3 14"},
				{"Харби Уоллбенгер", 	"2 8 19"},
				{"Мятный Джулеп", 		"1 4 6 17 20"},
				{"Отвертка", 			"2 6 19"},
				{"Маргарита", 			"6 12 15"},
				{"Гимлет", 				"16 18"}
			};

			for (int i = 0; i < items.Length/3; i++) {
				Item it = new Item ();
				it.Name = items [i, 0];
				it.Strength = int.Parse (items [i, 1]);
				it.Level = int.Parse (items [i, 2]);
				it.ImageName = "item" + i.ToString() + ".png";
				database.SaveItem (it);
			}

			for (int i = 0; i < drinks.Length/2; i++) {
				Drink dr = new Drink ();
				dr.Name = drinks [i, 0];
				dr.ImageName = "drink" + i.ToString () + ".png";
				dr.ingridients = drinks [i, 1];
				dr.Level = 0;
				foreach (String item in dr.ingridients.Split(' '))
					dr.Level += Int32.Parse(items [Int32.Parse (item) - 1, 1]) + Int32.Parse(items [Int32.Parse (item) - 1, 2]) * 10;
				database.SaveDrink (dr);
			}
		}
	}
}